<?php
/**
 * Created by PhpStorm.
 * User: p3dro
 * Date: 19/02/2019
 * Time: 4:01 PM
 */

namespace Tests;


use PedroLibrary\Functions;
use PHPUnit\Framework\TestCase;

class FunctionsTest extends TestCase
{
    public function testSum()
    {
        $array = array(
            array('price' => 5, 'stock' => 4),
            array('price' => 10, 'stock' => 8),
            array('price' => 15, 'stock' => 8),
        );
        $sum = Functions::sum($array, 'price');
        $this->assertSame(30, $sum);
    }

    public function testSumCallback()
    {
        $array = array(
            array('price' => 5, 'stock' => 1),
            array('price' => 10, 'stock' => 2),
            array('price' => 15, 'stock' => 3),
        );
        $sum = Functions::sum($array, function ($item) {
            return $item['price'] * $item['stock'];
        });
        $this->assertSame(70, $sum);
    }

    public function testSumRecursive()
    {
        $array = array(
            array('price' => 5, 'stock' => 1, 'hijos' => array(
                'price' => 10,
                'stock' => 2,
                'hijos' => array(
                    'price' => 15,
                    'stock' => 2,
                )
            )
            ),
        );
        $sum = Functions::sumRecursive($array, 'price');
        $this->assertSame(30, $sum);
    }
}