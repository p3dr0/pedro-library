<?php
/**
 * Created by PhpStorm.
 * User: p3dro
 * Date: 18/02/2019
 * Time: 5:46 PM
 */

namespace Tests;


use PedroLibrary\Functions;
use PHPUnit\Framework\TestCase;
use PedroLibrary\Credit;

class CreditTest extends TestCase
{

    protected $credit;

    public function __construct()
    {
        $this->credit = new Credit();
        $this->credit->setAmount(100);
        $this->credit->setCuotas(4);
    }

    public function testAmount()
    {


        $this->credit->setInterestRate(0);
        $result = $this->credit->generate();
        $this->assertNotEmpty($result);
        $this->assertSame(4, count($result));
        $this->assertEquals(25, $result[0]['monto_cuota']);
    }

    public function testInterestRate()
    {

        $this->credit->setInterestRate(10);
        $result = $this->credit->generate();
        $this->assertEquals(0.0003, $result[0]['tasa_cuota']);
        $this->assertEquals(25.02, $result[0]['monto_cuota']);
    }

    public function testDate()
    {
        $date = '2019-02-19';
        $this->credit->setDate($date);
        $day = $this->credit->nextDate($date, 'day', '1');
        $month = $this->credit->nextDate($date, 'month', '1');
        $year = $this->credit->nextDate($date, 'year', '1');
        $this->assertEquals('2019-02-20', $day);
        $this->assertEquals('2019-03-19', $month);
        $this->assertEquals('2020-02-19', $year);

    }

    public function testTotalAmount()
    {
        $this->credit->setInterestRate(10);
        $result = $this->credit->generate();
        $totalCuota = Functions::sum($result,'monto_cuota');
        $totalCapital = Functions::sum($result,'capital');
        $totalInterest = Functions::sum($result,'interes');
        $this->assertEquals($totalCuota, $totalCapital + $totalInterest);
    }

    public function testLeasing()
    {
        $this->credit->setTypeCalculation(2);
        $result = $this->credit->generate();
        $this->assertEquals(25 * 1.18, $result[0]['monto_cuota']);
    }

    public function testWithLeasing()
    {
        $purchase = 10;
        $this->credit->setTypeCalculation(2);
        $this->credit->with('first_letter', 1234);
        $this->credit->with('purchase_option', $purchase);
        $result = $this->credit->generate();
        $this->assertEquals(1234, $result[0]['numero_letra']);
        $this->assertEquals($purchase, $result[4]['monto_cuota']);

    }
    public function testAmericano()
    {
        $this->credit->setTypeCalculation(3);
        $this->credit->setAmount(10000);
        $this->credit->setInterestRate(3);
        $this->credit->setCuotas(5);
        $this->credit->setCycle('A');
        $this->credit->setDate(date("Y-m-d"));
        $result = $this->credit->generate();
        $this->assertEquals(300, $result[0]['interes']);
        $this->assertEquals(10300, $result[4]['monto_cuota']);
    }
}