<?php namespace PedroLibrary;


use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Facades\Excel;

define('TIPO_TEXTO', 1);
define('TIPO_NUMERO', 2);
define('TIPO_NUMERO_DECIMAL', 3);
define('TIPO_NUMERO_DECIMAL_2', 3);
define('TIPO_NUMERO_DECIMAL_3', 4);
define('TIPO_NUMERO_DECIMAL_4', 5);
define('TIPO_NUMERO_DECIMAL_5', 6);
define('TIPO_MONEDA', 7);
define('TIPO_FECHA', 8);
define('TIPO_FECHA_HORA', 9);

class JqGrid
{

    /*
     * * TIPOS DE COLUMNAS:
     *      -> TEXTO
     *      -> NUMERO
     *      -> MONEDA
     *      -> FECHA
     *      -> FECHA_HORA
     */

    protected $conBusqueda = false;
    protected $rows = 30;
    protected $page = 1;
    protected $totalRecords = 0;
    protected $columnaOrden = '';
    protected $orden = 'asc';
    protected $exporOper = null;
    protected $condicionGrilla = null;
    protected $filtros = [];

    protected $BD = null;
    protected $columnas = [];
    protected $idColumna = null;
    protected $request;
    protected $namespaceModel="App\\";
    protected $classExport=[];

    public function __construct()
    {
        $this->request = Request::capture();
        $this->rows = $this->request->input('rows',50);
        $this->page = $this->request->input('page',1);
        $this->columnaOrden = $this->request->input('sidx');
        $this->orden = $this->request->input('sord');
        $this->conBusqueda = $this->request->input('_search') == 'true';

        if ($this->request->has('oper')) {
            $this->exporOper = $this->request->input('oper');
        } else {
            $this->exporOper = null;
        }

        if ($this->conBusqueda == true) {

            if ($this->request->input('filters', null) == null) {

                $this->filtros[] = array('campo' => $this->request->input('searchField'), 'texto' => $this->request->input('searchString'), 'operacion' => $this->request->input('searchOper'), 'tipo' => 'AND');

            } else {

                /*if(function_exists("get_magic_quotes_gpc") && get_magic_quotes_gpc()){
                    $filtro = stripslashes($this->request->input('filters'));
                } else {
                    $filtro = $this->request->input('filters');
                }*/
                $filtro = stripslashes($this->request->input('filters'));

                $filtro = json_decode($this->prepareJSON(preg_replace('/\s+/'," ", $filtro)));
                $and_or =isset($filtro->groupOp)?$filtro->groupOp:'AND';
                $reglas = isset($filtro->rules)?$filtro->rules:array();

                foreach ($reglas as $value) {
                    $this->filtros[] = array(
                        'campo' => $value->field,
                        'texto' => $value->data,
                        'operacion' => $value->op,
                        'tipo' => $and_or);
                }

            }
        }
        // print_r(Input::all());
        $masFiltro = $this->request->all();
        unset($masFiltro['_search']);
        unset($masFiltro['nd']);
        unset($masFiltro['rows']);
        unset($masFiltro['page']);
        unset($masFiltro['sidx']);
        unset($masFiltro['sord']);
        unset($masFiltro['filters']);
        unset($masFiltro['modelo']);
        if ($this->request->filled('_between')) {
            $dates = explode(",", $this->request->input('_between'));
            foreach ($dates as $value) {
                $col = explode(":", $value);
                if (empty($col[1]) || empty($col[2])) {
                    continue;
                }
                $this->filtros[] = array(
                    'campo' => $col[0],
                    'texto' => $col[1] . '@' . $col[2],
                    'operacion' => 'between',
                    'mas_filtro' => 'S',
                    'tipo' => 'AND');
            }
            unset($masFiltro['_between']);
        }


        foreach ($masFiltro as $key => $value) {

            $this->filtros[] = array(
                'campo' => $key,
                'texto' => $value,
                'operacion' => 'eq',
                'mas_filtro' => 'S',
                'tipo' => 'AND');
        }

    }
    public function setClassExport($type,$class)
    {
        $this->classExport[$type]=$class;
        return $this;
    }

    public function setNamespaceModel($namespaceModel)
    {
        $this->namespaceModel=$namespaceModel;
    }
    private function crearBD()
    {

        if ($this->request->has('modelo')) {
            $parModelo = $this->request->input('modelo');

            $arrayModelo = explode(";", $parModelo);
            $relations = array();//para evitar el N+1
            if (is_array($arrayModelo)) {

                try {

                    $modelo = $arrayModelo[0];
                    $idTabla = $arrayModelo[1];
                    $columnas = explode(",", $arrayModelo[2]);
                    $conEstado = $arrayModelo[3];

                    foreach ($columnas as $value) {
                        $col = explode(":", $value);
                        $this->columnas[] = array('nombre' => $col[0], 'tipo' => $col[1], 'columna' => $col[2], 'solo_codigo' => $col[3], 'titulo' => $col[4]);
                        if ((strpos($col[0], 'id_') !== false || strpos($col[0], 'cod_') !== false || strpos($col[0], 'codigo_') !== false) && $idTabla != $col[0]) {
                            if ($col[3] == 'N') {
                                $relations[] = JqGridUtil::parseColumnRelated($col[0]);
                            }

                        }
                    }
                    array_push($this->columnas, array('nombre' => $idTabla, 'tipo' => TIPO_NUMERO, 'columna' => $idTabla, 'solo_codigo' => 'N', 'titulo' => $idTabla));


                    $filtroEstado = false;
                    foreach ($this->filtros as $value) {
                        if ($value['campo'] == 'estado') {
                            $filtroEstado = true;
                            break;
                        }
                    }


                    $this->idColumna = array('nombre' => $idTabla, 'tipo' => TIPO_NUMERO);
                    $this->BD = app("{$this->namespaceModel}{$modelo}");
                    if (count($relations) > 0) {
                        $this->BD = $this->BD->with($relations);
                    }
                    if ($conEstado == 'S' && $filtroEstado == false) {
                        $this->BD = $this->BD->where('estado', 'A');
                    }

                } catch (Exception $ex) {
                    \Log::error('Modelo no esta bien definido');
                }
            } else
                \Log::error('Modelo no valido');

        } else
            \Log::error('No existe modelo definido');

    }

    private function prepareJSON($input)
    {
        $imput = mb_convert_encoding($input, 'UTF-8', 'ASCII,UTF-8,ISO-8859-1');
        if (substr($input, 0, 3) == pack("CCC", 0xEF, 0xBB, 0xBF)) $input = substr($input, 3);
        return $input;
    }

    public function make($dbObj = null, $idcolumna = null, $columnas = null)
    {
        set_time_limit(0);
        if ($dbObj == null) {
            $this->crearBD();
        } else {
            $this->BD = $dbObj;
            $this->idColumna = $idcolumna;
            $this->columnas = $columnas;
        }

        return $this->build();

    }

    private function getDefinicionCampo($campo)
    {

        $resultado = array($campo, TIPO_TEXTO);

        foreach ($this->columnas as $value) {

            if ($value['nombre'] == $campo) {
                $resultado = array($value['nombre'], $value['tipo']);
                break;
            }

        }

        if (strpos($campo, 'id_') !== false) {
            $resultado = array($campo, TIPO_NUMERO);
        }
        return $resultado;
    }

    private function hasScope($query, $scope)
    {
        return method_exists($query->getModel(), $scope);
    }

    private function build()
    {
        set_time_limit(0);
        $band = 0;
        foreach ($this->filtros as $filtro) {

            if ($filtro['operacion'] == 'between') {
                list($from, $to) = explode('@', $filtro['texto'], 2);
                $from = Functions::convertDate('/', '-', $from);
                $to = Functions::convertDate('/', '-', $to);
                if ($from == $to) {
                    $this->BD = $this->BD->where($filtro['campo'], '=', $from);
                } else {
                    $this->BD = $this->BD->where($filtro['campo'], '>=', $from);
                    $this->BD = $this->BD->where($filtro['campo'], '<=', $to);
                }
                continue;
            }

            if ((strpos($filtro['campo'], 'id_') !== false || strpos($filtro['campo'], 'cod_') !== false || strpos($filtro['campo'], 'codigo_') !== false) && $this->idColumna['nombre'] != $filtro['campo'] && $this->conBusqueda == true && !isset($filtro['mas_filtro'])) {
                $band = 0;
                $col = collect($this->columnas)->where('nombre', $filtro['campo'])->values()->first();

                if (count($col) > 0 && is_array($col)) {
                    if ($col['solo_codigo'] == 'N') {
                        $band = 1;
                        $columna = JqGridUtil::parseColumnRelated($col['nombre']);
                        $operacion = 'LIKE';
                        $campofiltro = \DB::raw("upper(" . $col['columna'] . ")");
                        $texto = '%' . mb_strtoupper($filtro['texto']) . '%';
                        $scope = 'scope' . Str::studly($col['columna']);
                        if ($filtro['campo'] != 'oper') {
                            $this->BD = $this->BD->whereHas($columna, function ($query) use ($campofiltro, $operacion, $texto, $scope) {
                                if ($this->hasScope($query, $scope)) {
                                    $query->getModel()->{$scope}($query, $texto);
                                } else {
                                    $query->where($campofiltro, $operacion, $texto);
                                }

                            });
                        }
                        continue;
                    }
                }


            } else {
                $band = 0;
            }
            if ($band == 0) {
                $campo = $this->getDefinicionCampo($filtro['campo']);
                if($campo[1]==TIPO_FECHA){
                    $filtro['texto']=Functions::convertDate('/','-',$filtro['texto']);
                }

                if ($campo[1] == TIPO_TEXTO) {
                    $operacion = 'LIKE';
                    $texto = '%' . mb_strtoupper($filtro['texto']) . '%';
                    $campofiltro = \DB::raw("upper(" . $filtro['campo'] . ")");
                    if (Functions::validateDate($filtro['texto'])) {
                        $operacion = ' = ';
                        $texto = "'" . $filtro['texto'] . "'";
                        $campofiltro = $filtro['campo'];
                    }
                } else {
                    $campofiltro = $filtro['campo'];
                    $operacion = '=';
                    $texto=JqGridUtil::whereNumeric($filtro['texto'],$campo[1],[
                        TIPO_NUMERO, TIPO_NUMERO_DECIMAL,
                        TIPO_NUMERO_DECIMAL_2, TIPO_NUMERO_DECIMAL_3,
                        TIPO_NUMERO_DECIMAL_4, TIPO_NUMERO_DECIMAL_5,
                        TIPO_MONEDA
                    ]);
                }
                if ($filtro['campo'] != 'oper') {
                    $scope = 'scope' . Str::studly($filtro['campo']);
                    if ($this->hasScope($this->BD, $scope)) {
                        $this->BD = $this->BD->getModel()->{$scope}($this->BD, $texto);
                    } else {
                        if ($filtro['tipo'] == 'AND') {
                            $this->BD = $this->BD->where($campofiltro, $operacion, $texto);
                        } else {
                            $this->BD = $this->BD->orWhere($campofiltro, $operacion, $texto);
                        }
                    }

                }
            }
        }


        $this->totalRecords = $this->BD->count();
        if ($this->columnaOrden != '') {
            $this->BD = $this->BD->orderBy($this->columnaOrden, $this->orden);
        }
        if ($this->exporOper == null) {
            $datos = $this->BD->skip(($this->rows * $this->page) - $this->rows)->take($this->rows)->get();
        } else {
            $datos = $this->BD->get(array_column($this->columnas, 'nombre'));
        }
        $data = array();
        $idTabla = $this->idColumna['nombre'];
        if ($this->exporOper == null) {
            foreach ($datos as $value) {

                $columnas = array();
                foreach ($this->columnas as $col) {
                    $columnas[] = JqGridUtil::parseColumn($col, $value, $this->idColumna);
                }
                $data[] = array('id' => $value->$idTabla, 'cell' => $columnas);
            }
            return $this->renderJson($data);
        }

        return $this->export($datos);

    }

    public function renderJson($data)
    {
        if ($this->totalRecords > 0)
            $totalPage = ceil($this->totalRecords / $this->rows);
        else
            $totalPage = 0;
        return array(
            'page' => $this->page,
            'total' => (int)$totalPage,
            'records' => $this->totalRecords,
            'rows' => $data,
        );
    }

    public function export($datos)
    {
        set_time_limit(0);
        switch ($this->exporOper) {
            case 'pdf':
                $pdf = new JqGridPdf();
                $pdf->with('idColumna', $this->idColumna);
                $pdf->with('columns', $this->columnas);
                $pdf->with('datos', $datos);
                return $pdf->make();
                break;

            case 'excel':
                $classExport=isset($this->classExport['excel'])?$this->classExport['excel']:null;
                if(!is_null($classExport)){
                    $exp=new $classExport($this->idColumna, $this->columnas, collect($datos), false);
                    $exp->export();
                    return;
                }
                $excel = new JqGridExcel($this->idColumna, $this->columnas, $datos);
                return Excel::download($excel, 'expordata_' . date("YmdHis") . '.xlsx');

        }
    }



//_search:true / false si no busca
//nd:1376597226075
//rows:30
//page:1
//sidx:id
//sord:desc
//filters:{"groupOp":"AND","rules":[{"field":"descripcion","op":"eq","data":"eee"}]}

}
