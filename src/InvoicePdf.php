<?php namespace PedroLibrary;
/**
 * Created by PhpStorm.
 * User: p3dro
 * Date: 24/08/2018
 * Time: 7:53 PM
 */
class InvoicePdf extends Pdf
{
    private function resizeToFit($image, $maxImageDimensions)
    {
        list($width, $height) = getimagesize($image);
        $newWidth = $maxImageDimensions[0] / $width;
        $newHeight = $maxImageDimensions[1] / $height;
        $scale = min($newWidth, $newHeight);
        return array(
            round($this->pixelsToMM($scale * $width)),
            round($this->pixelsToMM($scale * $height))
        );
    }

    private function pixelsToMM($val)
    {
        $mm_inch = 25.4;
        $dpi = 96;
        return ($val * $mm_inch) / $dpi;
    }

    function Logo($logo, $maxImageDimensions = array(100, 60))
    {
        //$maxImageDimensions 	= array(100,60);
        $dimensions = $this->resizeToFit($logo, $maxImageDimensions);
        $this->Image($logo, 10, 5, $dimensions[0], $dimensions[1]);

    }

    function Enterprise($datos)
    {
        $this->Ln(12);
        $columns = array();
        foreach ($datos as $key => $value) {
            //$this->Row(array(utf8_encode($value['description']),":",$value['value']),false);
            $col = array();
            $col[] = array('text' => $value, 'width' => '128', 'height' => '5', 'align' => 'L', 'font_name' => 'Arial', 'font_size' => '9', 'font_style' => '', 'fillcolor' => '255,255,255', 'textcolor' => '0,0,0', 'drawcolor' => '0,0,0', 'linewidth' => '0.1', 'linearea' => '');
            $columns[] = $col;
        }
        $this->WriteTable($columns);
    }

    function Document($datos)
    {
        $this->setFont("Arial", "B", "11");
        $this->SetWidths(array(60));
        $this->SetAligns(array('C'));
        $this->SetXY($this->w - 70, 7);
        $pos = 0;
        foreach ($datos as $value) {
            $this->SetX($this->w - 70);
            $this->Row(array($value), false);
            $this->Ln();
        }
        $this->RoundedRect($this->w - 70, 6, 60, 32, 2, 'D');//ruc/factura
    }

    function Entity($datos)
    {
        $this->Ln();
        $this->RoundedRect(10, 40, $this->w - 81, 25, 2, 'D');//cliente
        $columns = array();
        foreach ($datos as $key => $value) {
            //$this->Row(array(utf8_encode($value['description']),":",$value['value']),false);
            $col = array();
            $col[] = array('text' => $value['description'], 'width' => '33', 'height' => '5', 'align' => 'L', 'font_name' => 'Arial', 'font_size' => '10', 'font_style' => 'B', 'fillcolor' => '255,255,255', 'textcolor' => '0,0,0', 'drawcolor' => '0,0,0', 'linewidth' => '0.1', 'linearea' => '');
            $col[] = array('text' => ':', 'width' => '4', 'height' => '5', 'align' => 'L', 'font_name' => 'Arial', 'font_size' => '10', 'font_style' => 'B', 'fillcolor' => '255,255,255', 'textcolor' => '0,0,0', 'drawcolor' => '0,0,0', 'linewidth' => '0.1', 'linearea' => '');
            $col[] = array('text' => $value['value'], 'width' => '91', 'height' => '5', 'align' => 'L', 'font_name' => 'Arial', 'font_size' => '9', 'font_style' => '', 'fillcolor' => '255,255,255', 'textcolor' => '0,0,0', 'drawcolor' => '0,0,0', 'linewidth' => '0.1', 'linearea' => '');
            $columns[] = $col;
        }
        $this->WriteTable($columns);
    }

    function DateCurrency($datos)
    {
        $this->Ln();
        $this->SetY(42);
        $this->RoundedRect($this->w - 70, 40, 60, 25, 2, 'D');//fecha

        $columns = array();
        foreach ($datos as $key => $value) {
            $col = array();
            $col[] = array('text' => '', 'width' => '131', 'height' => '5', 'align' => 'L', 'font_name' => 'Arial', 'font_size' => '10', 'font_style' => 'B', 'fillcolor' => '255,255,255', 'textcolor' => '0,0,0', 'drawcolor' => '0,0,0', 'linewidth' => '0.1', 'linearea' => '');
            $col[] = array('text' => $value['description'], 'width' => '22', 'height' => '5', 'align' => 'L', 'font_name' => 'Arial', 'font_size' => '10', 'font_style' => 'B', 'fillcolor' => '255,255,255', 'textcolor' => '0,0,0', 'drawcolor' => '0,0,0', 'linewidth' => '0.1', 'linearea' => '');
            $col[] = array('text' => ':', 'width' => '4', 'height' => '5', 'align' => 'L', 'font_name' => 'Arial', 'font_size' => '10', 'font_style' => 'B', 'fillcolor' => '255,255,255', 'textcolor' => '0,0,0', 'drawcolor' => '0,0,0', 'linewidth' => '0.1', 'linearea' => '');
            $col[] = array('text' => $value['value'], 'width' => '31', 'height' => '5', 'align' => 'L', 'font_name' => 'Arial', 'font_size' => '9', 'font_style' => '', 'fillcolor' => '255,255,255', 'textcolor' => '0,0,0', 'drawcolor' => '0,0,0', 'linewidth' => '0.1', 'linearea' => '');
            $columns[] = $col;
        }
        $this->WriteTable($columns);

    }

    function Columns($data,$fontSize='10',$bottom=100)
    {
        $this->setFont("Arial", "B", $fontSize);
        $nb = 0;
        for ($i = 0; $i < count($data); $i++)
            $nb = max($nb, $this->NbLines($this->widths[$i], $data[$i]));
        $h = 5 * $nb;
        //Issue a page break first if needed
        $this->CheckPageBreak($h);
        //Draw the cells of the row
        $colX = 10;
        for ($i = 0; $i < count($data); $i++) {
            $w = $this->widths[$i];
            $a = isset($this->aligns[$i]) ? $this->aligns[$i] : 'C';
            if ($this->si) {
                $a = $this->walign[$i];
            }
            //Save the current position
            $x = $this->GetX();
            $y = $this->GetY();
            $y2 = $this->h - $bottom - $y;
            $colX += $w;
            $this->MultiCell($w, 5, utf8_decode($data[$i]), 0, $a, false);
            if ($i < count($data) - 1) {
                $this->Line($colX, $y, $colX, $y + $y2);
            }
            $this->SetXY($x + $w, $y);
        }
        $r1 = 10;
        $r2 = $this->w - ($r1 * 2);
        $y1 = $this->GetY();
        $y2 = $this->h - $bottom - $y1;
        $this->SetXY($r1, $y1);
        $this->RoundedRect($r1, $y1, $r2, $y2, 2, "D");
        $this->Line($r1, $y1 + 10, $r1 + $r2, $y1 + 10);
        $this->Ln($h);
    }

    function Item($fields)
    {
        $this->SetAligns(array('L', 'L', 'L', 'R', 'R'));
        //$this->Ln();
        $this->Row($fields, false);
    }

    function Totales($datos, $simbolo, $position = 0)
    {
        $this->Ln();
        $setY = (int)($this->h - 98);
        if ($position > $setY) {
            $setY = $position + 4;
        }
        $this->SetY($setY);
        $this->RoundedRect($this->w - 75, $setY, 65, 40, 2, 'D');//fecha
        $columns = array();
        foreach ($datos as $key => $value) {
            $col = array();
            $col[] = array('text' => '', 'width' => '125', 'height' => '5', 'align' => 'L', 'font_name' => 'Arial', 'font_size' => '9', 'font_style' => 'B', 'fillcolor' => '255,255,255', 'textcolor' => '0,0,0', 'drawcolor' => '0,0,0', 'linewidth' => '0.1', 'linearea' => '');
            $col[] = array('text' => $value['description'], 'width' => '32', 'height' => '5', 'align' => 'L', 'font_name' => 'Arial', 'font_size' => '9', 'font_style' => 'B', 'fillcolor' => '255,255,255', 'textcolor' => '0,0,0', 'drawcolor' => '0,0,0', 'linewidth' => '0.1', 'linearea' => '');
            $col[] = array('text' => $simbolo, 'width' => '7', 'height' => '5', 'align' => 'L', 'font_name' => 'Arial', 'font_size' => '9', 'font_style' => 'B', 'fillcolor' => '255,255,255', 'textcolor' => '0,0,0', 'drawcolor' => '0,0,0', 'linewidth' => '0.1', 'linearea' => '');
            $col[] = array('text' => number_format($value['value'], 2), 'width' => '24', 'height' => '5', 'align' => 'R', 'font_name' => 'Arial', 'font_size' => '9', 'font_style' => '', 'fillcolor' => '255,255,255', 'textcolor' => '0,0,0', 'drawcolor' => '0,0,0', 'linewidth' => '0.1', 'linearea' => '');
            $columns[] = $col;
        }
        $this->WriteTable($columns);
    }

    function Note($note, $position = 0)
    {
        $this->Ln();
        $setY = (int)($this->h - 98);
        if ($position > $setY) {
            $setY = $position + 4;
        }
        $this->SetY($setY);
        $columns = array();
        $col = array();
        $col[] = array('text' => $note, 'width' => '120', 'height' => '5', 'align' => 'L', 'font_name' => 'Arial', 'font_size' => '9', 'font_style' => '', 'fillcolor' => '255,255,255', 'textcolor' => '0,0,0', 'drawcolor' => '0,0,0', 'linewidth' => '0.1', 'linearea' => '');
        $columns[] = $col;
        $this->WriteTable($columns);
    }

    function FooterNote($note)
    {
        $this->Ln();
        $columns = array();
        $col = array();
        $col[] = array('text' => $note, 'width' => '190', 'height' => '5', 'align' => 'C', 'font_name' => 'Arial', 'font_size' => '9', 'font_style' => '', 'fillcolor' => '255,255,255', 'textcolor' => '0,0,0', 'drawcolor' => '0,0,0', 'linewidth' => '0.1', 'linearea' => '');
        $columns[] = $col;
        $this->WriteTable($columns);
    }
}