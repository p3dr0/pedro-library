<?php
/**
 * Created by PhpStorm.
 * User: p3dro
 * Date: 18/02/2019
 * Time: 12:15 PM
 */

namespace PedroLibrary;


class Credit
{
    protected $data = array();
    protected $typeCalculation = 1;
    protected $interestRate = 0;
    protected $cycle = 'D';
    protected $cycleNumber = 1;
    protected $amount;
    protected $cuotas;
    protected $date;
    protected $safeRate = 0;
    protected $typeSafeRate = 'FIXED AMOUNT';
    protected $othersRate = 0;
    protected $typeOthersRate = 'FIXED AMOUNT';
    protected $igv = 0.18;

    /**
     * @param int $typeCalculation
     */
    public function setTypeCalculation($typeCalculation)
    {
        $this->typeCalculation = $typeCalculation;
    }

    /**
     * @param mixed $interestRate
     */
    public function setInterestRate($interestRate)
    {
        $this->interestRate = $interestRate;
    }

    /**
     * @param mixed $cycle
     */
    public function setCycle($cycle)
    {
        $this->cycle = $cycle;
    }

    /**
     * @param int $cycleNumber
     */
    public function setCycleNumber($cycleNumber)
    {
        $this->cycleNumber = $cycleNumber;
    }

    /**
     * @param mixed $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * @param mixed $cuotas
     */
    public function setCuotas($cuotas)
    {
        $this->cuotas = $cuotas;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @param mixed $safeRate
     */
    public function setSafeRate($safeRate)
    {
        $this->safeRate = $safeRate;
    }

    /**
     * @param mixed $typeSafeRate
     */
    public function setTypeSafeRate($typeSafeRate)
    {
        $this->typeSafeRate = $typeSafeRate;
    }

    /**
     * @param mixed $othersRate
     */
    public function setOthersRate($othersRate)
    {
        $this->othersRate = $othersRate;
    }

    /**
     * @param mixed $typeOthersRate
     */
    public function setTypeOthersRate($typeOthersRate)
    {
        $this->typeOthersRate = $typeOthersRate;
    }

    /**
     * @param float $igv
     */
    public function setIgv($igv)
    {
        $this->igv = $igv;
    }


    public function with($key, $value = null)
    {
        if (is_array($key)) {
            $this->data[] = $key;
        }
        $this->data[$key] = $value;
    }

    public function generate()
    {

        $date = Functions::convertDate('/', '-', $this->date);
        $nominalRate = Functions::nominalRate($this->cycle, $this->cycleNumber, $this->interestRate);
        $cuotaRate = $nominalRate[0];
        $interval = $nominalRate[1];
        $result = array();
        if ($this->typeCalculation == 1) {
            $result = $this->frances($cuotaRate, $date, $interval);
        }

        if ($this->typeCalculation == 2) {
            $result = $this->leasing($cuotaRate, $date, $interval);
        }
        if ($this->typeCalculation == 3) {
            $result = $this->americano($cuotaRate, $date, $interval);
        }
        return $result;
    }

    public function frances($cuotaRate, $date, $interval)
    {
        try {
            $rate = (pow($cuotaRate + 1, $this->cuotas) * $cuotaRate) / ((pow($cuotaRate + 1, $this->cuotas)) - 1);
            $amountCuota = round($this->amount * $rate, 4);
        } catch (\Exception $ex) {
            $amountCuota = round($this->amount / $this->cuotas, 4);
        }
        if (!empty($this->data['fixed_cuota'])) {
            $amountCuota = $this->data['fixed_cuota'];
        }
        $calcular_itf = 'N';
        $capitalResidue = $this->amount;
        $sumDate = 0;
        $startDate = $date;
        $itf = 0;
        $letter = (!empty($this->data['first_letter'])) ? $this->data['first_letter'] : "";
        $numberCuota = (!empty($this->data['initial_index'])) ? $this->data['initial_index'] : 0;
        $result = array();
        for ($i = 0; $i < $this->cuotas; $i++) {
            $sumDate = $this->cycleNumber + $sumDate;
            $interest = round($capitalResidue * $cuotaRate, 2);
            $capital = round($amountCuota - $interest, 2);
            $amountSafe = $this->safeRate;
            if ($this->typeSafeRate == 'PERCENT') {
                $amountSafe = round($capitalResidue * $amountSafe / 100, 2);
            }
            if (($i + 1) == $this->cuotas && empty($this->data['fixed_cuota'])) {
                $capital = $capitalResidue;
                $amountCuota = round($interest + $capital, 2);
                $capitalResidue = 0;
            } else {
                $capitalResidue = round($capitalResidue - $capital, 2);
            }
            $amountOthers = $this->othersRate;
            if ($this->typeOthersRate == 'PERCENT') {
                $amountOthers = round($amountCuota * $amountOthers / 100, 2);
            }
            if ($calcular_itf == 'S')
                $itf = round(($amountCuota + $amountSafe + $amountOthers) * $this->data['tasa_itf'] / 100, 2);


            $c = array(
                'cuota' => ($numberCuota + 1),
                'monto_cuota' => round($amountCuota, 2),
                'interes' => round($interest, 2),
                'capital' => $capital,
                'capital_residue' => round($capitalResidue, 2),
                'fecha_pago' => $date,
                'seguro' => round($amountSafe, 2),
                'otros' => round($amountOthers, 2),
                'itf' => round($itf, 2),
                'tasa_cuota' => round($cuotaRate, 4),
                'merced' => 0,
                'igv' => 0,
                'numero_letra' => $letter
            );
            $date = $this->nextDate($startDate, $interval, $sumDate);
            $letter = (!empty($letter)) ? ($letter + 1) : "";
            $result[] = $c;
            $numberCuota++;
        }
        return $result;
    }

    public function leasing($cuotaRate, $date, $interval)
    {
        $capital = $this->amount;
        $initialPercent = (empty($this->data['percent_initial'])) ? 0 : $this->data['percent_initial'];
        $igv = 0.18;
        $initialCuota = round($initialPercent * $capital / 100, 2);
        $initialCuota = round($initialCuota * (1 + $igv), 2);
        $constante = $capital - $initialCuota;
        try {
            $kk = pow((1 - pow((1 + $cuotaRate), -$this->cuotas)) / $cuotaRate, -1);
            $merced = round($constante * $kk, 4);
        } catch (\Exception $ex) {
            $merced = round($constante / $this->cuotas, 4);
        }
        $amountCuota = round($merced * (1 + $igv), 4);
        if (!empty($this->data['fixed_cuota'])) {
            $amountCuota = $this->data['fixed_cuota'];
        }
        $sumDate = 0;
        $result = array();
        $capitalResidue = round($capital - $initialCuota, 2);
        $startDate = $date;
        $itf = 0;
        $letter = (!empty($this->data['first_letter'])) ? $this->data['first_letter'] : "";

        for ($i = 0; $i < $this->cuotas; $i++) {
            $sumDate = $this->cycleNumber + $sumDate;
            $interest = round($capitalResidue * $cuotaRate, 2);
            $capital = round($merced - $interest, 2);
            $amountSafe = $this->safeRate;
            if ($this->typeSafeRate == 'PORCENTAJE') {
                $amountSafe = round($capitalResidue * $amountSafe / 100, 2);
            }
            if (($i + 1) == $this->cuotas && empty($this->data['fixed_cuota'])) {
                $capital = $capitalResidue;
                $merced = round($interest + $capital, 2);
                $amountCuota = round($merced * (1 + $igv), 2);
                $capitalResidue = 0;
            } else {
                $capitalResidue = round($capitalResidue - $capital, 2);
            }
            $amountOthers = $this->othersRate;
            if ($this->typeOthersRate == 'PORCENTAJE') {
                $amountOthers = round($amountCuota * $amountOthers / 100, 2);
            }

            $c = array(
                'cuota' => ($i + 1),
                'monto_cuota' => round($amountCuota, 2),
                'interes' => round($interest, 2),
                'amortizacion' => $capital,
                'capital_residue' => round($capitalResidue, 2),
                'fecha_pago' => $date,
                'seguro' => round($amountSafe, 2),
                'otros' => round($amountOthers, 2),
                'itf' => round($itf, 2),
                'tasa_cuota' => round($cuotaRate, 4),
                'merced' => round($merced, 2),
                'igv' => ($igv + 1),
                'numero_letra' => $letter
            );
            $date = $this->nextDate($startDate, $interval, $sumDate);
            $letter = (!empty($letter)) ? ($letter + 1) : "";
            $result[] = $c;

        }
        if (!empty($this->data['purchase_option'])) {
            $merced = round($this->data['purchase_option'] / (1 + $igv), 4);

            $c = array(
                'cuota' => ($i + 1),
                'monto_cuota' => $this->data['purchase_option'],
                'interes' => 0,
                'amortizacion' => 0,
                'capital' => 0,
                'fecha_pago' => $date,
                'seguro' => 0,
                'otros' => 0,
                'itf' => 0,
                'tasa_cuota' => round($cuotaRate, 4),
                'merced' => round($merced, 2),
                'igv' => ($igv + 1),
                'numero_letra' => $letter
            );
            $result[] = $c;
        }
        return $result;
    }
    public function americano($cuotaRate, $date, $interval)
    {
        $result = array();
        $calcular_itf = 'N';
        $capitalResidue = $this->amount;
        $sumDate = 0;
        $startDate = $date;
        $itf = 0;
        $letter = (!empty($this->data['first_letter'])) ? $this->data['first_letter'] : "";
        $numberCuota = (!empty($this->data['initial_index'])) ? $this->data['initial_index'] : 0;
        $capital=0;
        $interest=round($cuotaRate*$capitalResidue,2);
        $amountCuota=$interest;
        for ($i = 0; $i < $this->cuotas; $i++) {
            $sumDate = $this->cycleNumber + $sumDate;
            $amountSafe = $this->safeRate;
            if ($this->typeSafeRate == 'PERCENT') {
                $amountSafe = round($capitalResidue * $amountSafe / 100, 2);
            }

            if (($i+1) == $this->cuotas){
                $capital = $capitalResidue;
                $amountCuota = round($interest + $capital,2);
                $capitalResidue = 0;
            }else{
                $capitalResidue = round($capitalResidue - $capital,2);
            }
            $amountOthers = $this->othersRate;
            if ($this->typeOthersRate == 'PERCENT') {
                $amountOthers = round($amountCuota * $amountOthers / 100, 2);
            }
            if ($calcular_itf == 'S')
                $itf = round(($amountCuota + $amountSafe + $amountOthers) * $this->data['tasa_itf'] / 100, 2);

            $c = array(
                'cuota' => ($numberCuota + 1),
                'monto_cuota' => round($amountCuota, 2),
                'interes' => round($interest, 2),
                'capital' => $capital,
                'capital_residue' => round($capitalResidue, 2),
                'fecha_pago' => $date,
                'seguro' => round($amountSafe, 2),
                'otros' => round($amountOthers, 2),
                'itf' => round($itf, 2),
                'tasa_cuota' => round($cuotaRate, 4),
                'merced' => 0,
                'igv' => 0,
                'numero_letra' => $letter
            );
            $date = $this->nextDate($startDate, $interval, $sumDate);
            $letter = (!empty($letter)) ? ($letter + 1) : "";
            $result[] = $c;
            $numberCuota++;
        }
        return $result;
    }

    public function nextDate($date, $interval, $sumDate)
    {
        $f = new \DateTime($date . " 00:00:00.000000");
        $f->modify('+' . $sumDate . ' ' . $interval);
        return $f->format('Y-m-d');
    }
}