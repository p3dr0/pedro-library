<?php namespace PedroLibrary;

use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Maatwebsite\Excel\Facades\Excel;

define('TIPO_TEXTO', 1);
define('TIPO_NUMERO', 2);
define('TIPO_NUMERO_DECIMAL', 3);
define('TIPO_NUMERO_DECIMAL_2', 3);
define('TIPO_NUMERO_DECIMAL_3', 4);
define('TIPO_NUMERO_DECIMAL_4', 5);
define('TIPO_NUMERO_DECIMAL_5', 6);
define('TIPO_MONEDA', 7);
define('TIPO_FECHA', 8);
define('TIPO_FECHA_HORA', 9);

class JqGridBD
{

    /*
     * * TIPOS DE COLUMNAS:
     *      -> TEXTO
     *      -> NUMERO
     *      -> MONEDA
     *      -> FECHA
     *      -> FECHA_HORA
     */

    protected $conBusqueda = false;
    protected $rows = 30;
    protected $page = 1;
    protected $totalRecords = 0;
    protected $columnaOrden = '';
    protected $orden = 'asc';
    protected $exporOper = null;
    protected $condicionGrilla = null;
    protected $filtros = [];

    protected $BD = null;
    protected $columnas = [];
    protected $idColumna = null;
    protected $sqlSelect = null;
    protected $sqlfrom = null;
    protected $sqlWhere = null;
    protected $sqlOrder = null;
    protected $sql;
    protected $condicionesAnd = [];
    protected $condicionesOr = [];
    protected $datosFooter = [];
    protected $request;
    protected $configPdf=[];
    protected $classExport=[];

    public function __construct()
    {
        $this->configPdf=array('P','mm','A4');
        $this->request = Request::capture();
        $this->rows = $this->request->input('rows',50);
        $this->page = $this->request->input('page',1);
        $this->columnaOrden = $this->request->input('sidx');
        $this->orden = $this->request->input('sord');
        $this->conBusqueda = $this->request->input('_search') == 'true';

        if ($this->request->has('oper')) {
            $this->exporOper = $this->request->input('oper');
        } else {
            $this->exporOper = null;
        }

        if ($this->conBusqueda == true) {
            if ($this->request->input('filters', null) == null) {
                $this->filtros[] = array('campo' => $this->request->input('searchField'), 'texto' => $this->request->input('searchString'), 'operacion' => $this->request->input('searchOper'), 'tipo' => 'AND');

            } else {

                /*if(function_exists("get_magic_quotes_gpc") && get_magic_quotes_gpc()){
                  $filtro = stripslashes($this->request->input('filters'));
                } else {
                    $filtro = $this->request->input('filters');
                }*/
                $filtro = stripslashes($this->request->input('filters'));

                $filtro = json_decode($this->prepareJSON(preg_replace('/\s+/'," ", $filtro)));
                $and_or =isset($filtro->groupOp)?$filtro->groupOp:'AND';
                $reglas = isset($filtro->rules)?$filtro->rules:array();

                foreach ($reglas as $value) {
                    $this->filtros[] = array(
                        'campo' => $value->field,
                        'texto' => $value->data,
                        'operacion' => $value->op,
                        'tipo' => $and_or);
                }
            }
        }
    }
    public function setClassExport($type,$class)
    {
        $this->classExport[$type]=$class;
        return $this;
    }
    public  function setConfigPdf(array $config)
    {
        $this->configPdf=$config;
    }
    private function crearBD()
    {
        if ($this->request->has('modelo')) {

            $parModelo = $this->request->input('modelo');
            $arrayModelo = explode(";", $parModelo);

            if (is_array($arrayModelo)) {

                try {

                    $modelo = $arrayModelo[0];
                    $idTabla = $arrayModelo[1];
                    $columnas = explode(",", $arrayModelo[2]);
                    $conEstado = $arrayModelo[3];

                    foreach ($columnas as $value) {
                        $col = explode(":", $value);
                        $this->columnas[] = array('nombre' => $col[0], 'tipo' => $col[1], 'columna' => $col[2], 'solo_codigo' => $col[3], 'titulo' => $col[4]);
                    }
                    $idTitulo=ucwords(str_replace(array('-', '_'), ' ', $idTabla));
                    array_push($this->columnas, array('nombre' => $idTabla, 'tipo' => TIPO_NUMERO, 'columna' => $idTabla, 'solo_codigo' => 'N', 'titulo' => $idTitulo));

                    $filtroEstado = false;

                    foreach ($this->filtros as $value) {
                        if ($value['campo'] == 'estado') {
                            $filtroEstado = true;
                            break;
                        }
                    }

                    if ($conEstado == 'S' && $filtroEstado == false) {
                        $this->filtros[] = array(
                            'campo' => 'estado',
                            'texto' => 'A',
                            'operacion' => 'eq',
                            'tipo' => 'AND');
                    }

                    $this->idColumna = array('nombre' => $idTabla, 'tipo' => TIPO_NUMERO);

                } catch (\Exception $ex) {
                    \Log::error('Modelo no esta bien definido');
                }
            } else
                \Log::error('Modelo no valido');

        } else
            \Log::error('No existe modelo definido');

    }

    private function prepareJSON($input)
    {
        $imput = mb_convert_encoding($input, 'UTF-8', 'ASCII,UTF-8,ISO-8859-1');
        if (substr($input, 0, 3) == pack("CCC", 0xEF, 0xBB, 0xBF)) $input = substr($input, 3);
        return $input;
    }

    public function make($dbObj = null, $idcolumna = null, $columnas = null)
    {
        set_time_limit(0);
        if ($dbObj == null) {
            $this->crearBD();
        } else {
            $this->BD = $dbObj;
            $this->idColumna = $idcolumna;
            $this->columnas = $columnas;
        }

        return $this->build();
    }

    private function getDefinicionCampo($campo)
    {
        $resultado = array($campo, TIPO_TEXTO);

        foreach ($this->columnas as $value) {
            if ($value['nombre'] == $campo) {
                $resultado = array($value['nombre'], $value['tipo']);
                break;
            }
        }
        if (strpos($campo, 'id_') !== false) {
            $resultado = array($campo, TIPO_NUMERO);
        }
        return $resultado;
    }


    public function select($sql)
    {
        $this->sqlSelect = $sql;
    }

    public function from($sql)
    {
        $this->sqlfrom = $sql;
    }

    public function whereRaw($sql)
    {
        $this->sqlWhere = " " . $sql;
    }

    public function where($column, $operator = null, $value = null, $boolean = 'and')
        //where($property, $value = null,$boolean='and')
    {
        if (func_num_args() == 2) {
            list($value, $operator) = array($operator, '=');
        }
        $property = $column . $operator;
        if (strtolower($boolean) == 'and') {
            $this->condicionesAnd[] = $property . $value;
        } else {
            $this->condicionesOr[] = $property . $value;
        }

    }

    public function orderByRaw($sql)
    {
        $this->sqlOrder = $sql;
    }

    public function compileSql()
    {
        $sqlWhereOrAux = '';
        $op = '';
        if ($this->sqlWhere == null)
            $this->sqlWhere = '';
        foreach ($this->condicionesOr as $value) {
            $sqlWhereOrAux = $value . $op . $sqlWhereOrAux;
            $op = ' or ';
        }
        $sqlWhereAndAux = '';
        $op = '';

        foreach ($this->condicionesAnd as $value) {
            $sqlWhereAndAux = $value . $op . $sqlWhereAndAux;
            $op = ' and ';

        }
        if (strlen(trim($sqlWhereAndAux)) > 0 && strlen(trim($this->sqlWhere)) > 0) {
            $sqlWhereAndAux = $sqlWhereAndAux . ' and';
        }

        if (strlen(trim($sqlWhereOrAux)) > 0 && (strlen(trim($sqlWhereAndAux)) > 0 || strlen(trim($this->sqlWhere)) > 0)) {
            $sqlWhereOrAux = ' (' . $sqlWhereOrAux . ') and';
        }
        $where = $sqlWhereOrAux . $sqlWhereAndAux . $this->sqlWhere;
        if (strlen(trim($where)) > 0) {
            $where = ' where ' . $where;
        }
        $order = "";
        if ($this->columnaOrden != '') {
            $this->columnaOrden = $this->columnaOrden . " " . $this->orden;
        }
        if ($this->sqlOrder == null)
            $this->sqlOrder = '';

        if (strlen($this->columnaOrden) > 0 && strlen($this->sqlOrder) > 0) {
            $this->columnaOrden .= ",";
        }
        $order = $this->columnaOrden . $this->sqlOrder;

        if (strlen(trim($order)) > 0) {
            $order = ' order by ' . $order;
        }

        $from = '';
        if (strlen(trim($where)) > 0) {
            $from = $this->sqlfrom;
        }
        $this->sqlWhere = $where;
        $this->sqlSelect = $this->sqlSelect . $from . $this->sqlWhere;
        if ($this->exporOper == null) {
            $this->sql = $this->sqlSelect . $order . " limit " . $this->rows . " offset " . ($this->rows * ($this->page - 1)) . "";
        } else {
            $this->sql = $this->sqlSelect . $order . " ";
        }
        $this->BD = \DB::select($this->sql);
        $this->totalRecords = \DB::select("select count(*) as cantidad from(" . $this->sqlSelect . $order . ") as tabla")[0]->cantidad;
    }

    public function toSql()
    {
        return $this->sql;
    }

    public function addDatosFooter($dato)
    {
        $this->datosFooter[] = $dato;
    }

    private function build()
    {
        set_time_limit(0);
        foreach ($this->filtros as $filtro) {

            $campofiltro = $filtro['campo'];
            $campo = $this->getDefinicionCampo($filtro['campo'], $filtro['texto']);
            if($campo[1]==TIPO_FECHA){
                $filtro['texto']=Functions::convertDate('/','-',$filtro['texto']);
                if (!Functions::validateDate($filtro['texto'],'Y-m-d')) {
                    throw ValidationException::withMessages([
                        'el formato no es valido para el campo fecha',
                    ]);
                }
            }
            if ($campo[1] == TIPO_TEXTO) {
                $operacion = ' LIKE ';
                $texto = \DB::connection()->getPdo()->quote("%" . mb_strtoupper($filtro['texto']) . "%");
                $campofiltro="upper(".$filtro['campo'].")";
                if (Functions::validateDate($filtro['texto'])) {
                    $operacion = ' = ';
                    $texto = "'" . $filtro['texto'] . "'";
                    $campofiltro = $filtro['campo'];
                }
            } else {
                $campofiltro = $filtro['campo'];
                $operacion = ' = ';
                $texto=JqGridUtil::whereNumeric($filtro['texto'],$campo[1],[
                    TIPO_NUMERO, TIPO_NUMERO_DECIMAL,
                    TIPO_NUMERO_DECIMAL_2, TIPO_NUMERO_DECIMAL_3,
                    TIPO_NUMERO_DECIMAL_4, TIPO_NUMERO_DECIMAL_5,
                    TIPO_MONEDA
                ]);
            }
            if ($filtro['tipo'] == 'AND') {
                $this->where($campofiltro, $operacion, $texto, 'and');


            } else {
                $this->where($campofiltro, $operacion, $texto, 'or');

            }
        }
        $this->compileSql();

        $datos = $this->BD;
        $data = array();
        $idTabla = $this->idColumna['nombre'];
        if ($this->exporOper == null) {
            foreach ($datos as $value) {
                $columnas = array();
                foreach ($this->columnas as $col) {
                    $columnas[] = JqGridUtil::parseColumn($col, $value, $this->idColumna, false);
                }
                $data[] = array('id' => $value->$idTabla, 'cell' => $columnas);
            }
        }

        $userdata = array();
        if (count($this->datosFooter) > 0) {
            for ($index1 = 0; $index1 < count($this->datosFooter); $index1++) {
                $dato = $this->datosFooter[$index1];
                if ($dato['Tipo'] == 'T') {
                    $userdata[$dato['Columna']] = "<span style='color:blue;'>" . $dato['Dato'] . "</span>";
                }
                if ($dato['Tipo'] == 'R') {
                    $userdata[$dato['Columna']] = "<span style='color: rgba(222, 0, 231, 0.99);font-weight: 700;font-size: 15px;'>" . $dato['Dato'] . "</span>";
                }
                if ($dato['Tipo'] == 'S') {
                    $cond = "";
                    if (strlen(trim($dato['Condicion'])) > 0) {
                        if (strlen(trim($this->sqlWhere)) > 0)
                            $cond = " and " . $dato['Condicion'];
                        else
                            $cond = $dato['Condicion'];
                    }
                    $sql = "select sum(" . $dato['Dato'] . ") as suma  from(" . $this->sqlSelect . $cond . ") as tabla";
                    $rst = \DB::select($sql);
                    $v = '0';
                    if (isset($rst[0]->suma))
                        $v = $rst[0]->suma;
                    $v = round(floatval($v), 2);
                    if (empty($dato['tipo_columna'])) {
                        $v = number_format($v, 2, '.', ',');
                        $userdata[$dato['Columna']] = "<span style='color:red;'>" . $v . "</span>";
                    } else {
                        $userdata[$dato['Columna']] = $v;
                    }

                }
            }
        }
        if ($this->exporOper == null) {
            return $this->renderJson($data);
        }
        return $this->export($datos);

    }

    public function renderJson($data)
    {
        if ($this->totalRecords > 0)
            $totalPage = ceil($this->totalRecords / $this->rows);
        else
            $totalPage = 0;
        return array(
            'page' => $this->page,
            'total' => (int)$totalPage,
            'records' => $this->totalRecords,
            'rows' => $data,
        );
    }

    public function export($datos)
    {
        set_time_limit(0);
        switch ($this->exporOper) {
            case 'pdf':
                list($orientation,$unit, $format)=$this->configPdf;
                $pdf = new JqGridPdf($orientation,$unit, $format);
                $pdf->setModel(false);
                $pdf->with('idColumna', $this->idColumna);
                $pdf->with('columns', $this->columnas);
                $pdf->with('datos', $datos);
                return $pdf->make();
                break;

            case 'excel':
                $classExport=isset($this->classExport['excel'])?$this->classExport['excel']:null;
                if(!is_null($classExport)){
                    $exp=new $classExport($this->idColumna, $this->columnas, collect($datos), false);
                    $exp->export();
                    return;
                }
                $excel = new JqGridExcel($this->idColumna, $this->columnas, collect($datos), false);
                return Excel::download($excel, 'expordata_' . date("YmdHis") . '.xlsx');

        }
    }

//_search:true / false si no busca
//nd:1376597226075
//rows:30
//page:1
//sidx:id
//sord:desc
//filters:{"groupOp":"AND","rules":[{"field":"descripcion","op":"eq","data":"eee"}]}

}
