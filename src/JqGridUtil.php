<?php
/**
 * Created by PhpStorm.
 * User: p3dro
 * Date: 26/11/2018
 * Time: 5:23 PM
 */

namespace PedroLibrary;

use Illuminate\Validation\ValidationException;

class JqGridUtil
{
    public static function parseColumnRelated($column)
    {
        $column = str_replace('id_', '', $column);
        $column = str_replace('cod_', '', $column);
        $column = str_replace('codigo_', '', $column);
        $column = str_replace('_', ' ', $column);

        $column = ucwords($column);
        $column = str_replace(' ', '', $column);
        return $column;
    }

    public static function extractColumnName($column)
    {
        if (strpos($column, '.')) {
            $array = explode('.', $column);
            return array_pop($array);
        }
        return $column;
    }

    public static function parseColumn($col, $value, $idColumna, $model = true)
    {
        $column = '';
        $columName = self::extractColumnName($col['nombre']);
        if ((strpos($columName, 'id_') !== false || strpos($columName, 'cod_') !== false || strpos($columName, 'codigo_') !== false) && $idColumna['nombre'] != $columName && $model) {

            if ($col['solo_codigo'] == 'N') {
                $columna = self::parseColumnRelated($columName);
                $column = $value->{$columna}->{$col['columna']};
            } else {
                $column = $value->{$columName};
            }
        } else {

            if (strpos($columName, 'sumar') !== false) {

                $dato = str_replace('sumar(', '', $columName);
                $dato = str_replace(')', '', $dato);
                $auxCol = explode('=', $dato);
                $sum = 0;
                foreach ($auxCol as $colval) {
                    $sum = $sum + floatval($value->$colval);
                }
                $column = $sum;
            } else if (strpos($columName, 'multiplicar') !== false) {
                $dato = str_replace('multiplicar(', '', $columName);
                $dato = str_replace(')', '', $dato);
                $auxCol = explode('=', $dato);
//
                $sum = 1;

                foreach ($auxCol as $colval) {
                    $sum = $sum * floatval($value->$colval);
                }
                $column = $sum;
            } else {
                if (gettype($value->{$columName}) == 'object' && get_class($value->{$columName}) == 'Carbon\Carbon') {
                    $obj = $value->{$columName};
                    $column = $obj->toDateTimeString();

                } else {
                    $column = ($model) ? $value->getAlias($columName, $value->{$columName}) : $value->{$columName};
                }
            }

        }
        return $column;
    }
    public static function whereNumeric($texto,$tipo,$validacion=[])
    {

        if (in_array($tipo, $validacion)) {
            if(!is_numeric($texto)){
                throw ValidationException::withMessages([
                    'el campo ingresado tiene que ser un número',
                ]);
            }
            return $texto;
        }
        return "'" . $texto . "'";
    }
}
