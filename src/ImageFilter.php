<?php
/**
 * Created by PhpStorm.
 * User: p3dro
 * Date: 13/09/2018
 * Time: 8:26 PM
 */

namespace PedroLibrary;


class ImageFilter
{

    public static function base64($imagecode)
    {
        $content = base64_encode($imagecode);
        return 'data:image/png;base64,' . $content;
    }
}