<?php
/**
 * Created by PhpStorm.
 * User: p3dro
 * Date: 23/11/2018
 * Time: 6:34 PM
 */

namespace PedroLibrary;


use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class JqGridExcel implements FromCollection, WithMapping, WithHeadings, ShouldAutoSize
{
    protected $columns = array();
    protected $data = array();
    protected $userdata = array();
    protected $model = true;

    public function __construct($idColumna, $columns, $datos, $model = true, $userdata = array())
    {
        $this->idColumna = $idColumna;
        $this->columns = $columns;
        $this->datos = $datos;
        $this->userdata = $userdata;
        $this->model = $model;
    }

    public function headings(): array
    {
        return array_column($this->columns, 'titulo');
    }

    public function map($row): array
    {
        $fields = array();
        $fields = array();
        foreach ($this->columns as $col) {
            $field = JqGridUtil::parseColumn($col, $row, $this->idColumna, $this->model);
            if ($col['tipo'] == 8) {
                $field = Functions::convertDate('-', '/', $field);
            }
            $fields[] = $field;
        }
        return $fields;
    }

    public function collection()
    {
        return $this->datos;
    }
}