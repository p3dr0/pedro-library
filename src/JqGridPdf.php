<?php namespace PedroLibrary;
/**
 * Created by PhpStorm.
 * User: p3dro
 * Date: 20/08/2018
 * Time: 3:31 PM
 */

use PedroLibrary\Pdf;

class JqGridPdf
{
    protected $pdf;
    protected $data = array();
    protected $model = true;
    protected $pageWidth=190;

    public function __construct($orientation = 'P', $unit = 'mm', $format = 'A4')
    {
        ob_get_clean();

        $this->pdf = new Pdf($orientation,$unit, $format);
        $this->pdf->SetMargins(10, 10, 10);
        $this->pdf->SetAutoPageBreak(true, 1);
        if($orientation=='L'){
            $this->pageWidth=$this->pdf->getPageWidth()-20;
        }
    }

    public function setModel($model)
    {
        $this->model = $model;
    }

    public function with($key, $value)
    {
        $this->data[$key] = $value;
    }

    public function columns()
    {
        $totalColumns = count($this->data['columns']);
        $width = ceil($this->pageWidth / $totalColumns);
        $widths = array();
        $title = array();
        foreach ($this->data['columns'] as $key => $item) {
            $widths[] = $width;

            $title[] = $item['titulo'];
        }
        return array($widths, $title);
    }

    public function make()
    {
        set_time_limit(0);

        $columns = $this->columns();
        $this->pdf->AliasNbPages();
        $this->pdf->enableheader = function ($pdf) use ($columns) {
            $pdf->SetWidths($columns[0]);
            $pdf->SetFillColor(82, 151, 199);
            $pdf->SetTextColor(0, 0, 0);
            $pdf->SetDrawColor(122, 122, 122);
            $pdf->SetFont('Arial', 'B', 10);
            $pdf->Row($columns[1]);

        };
        $this->pdf->enablefooter = function ($pdf) {
            $pdf->SetTextColor(0, 0, 0);
            $pdf->SetY(-10);
            $pdf->SetFont('Arial', 'B', 8);
            $pdf->Line(10, $pdf->GetY(), 200, $pdf->GetY());
            $pdf->Cell(30, 4, date('d/m/Y'), 0, 0, 'L');
            $pdf->Cell(163, 4, 'Pagina ' . $pdf->PageNo() . ' de {nb}', 0, 0, 'R');
        };
        $this->pdf->AddPage();
        $this->pdf->SetFillColor(255, 255, 255);
        $this->pdf->SetDrawColor(122, 122, 122);
        $this->pdf->SetFont('Arial', '', 7);
        $this->pdf->SetWidths($columns[0]);
        foreach ($this->data['datos'] as $value) {
            $fields = array();
            foreach ($this->data['columns'] as $col) {
                $field = JqGridUtil::parseColumn($col, $value, $this->data['idColumna'], $this->model);
                if ($col['tipo'] == 8) {
                    $field = Functions::convertDate('-', '/', $field);
                }
                $fields[] = $field;
            }
            $this->pdf->Row($fields);

        }
        $this->pdf->Output('D', 'exportdata_' . date("YmdHis") . '.pdf');

    }
}