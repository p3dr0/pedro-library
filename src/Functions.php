<?php namespace PedroLibrary;
class Functions
{

    public static function urlActive($isReferer = false)
    {
        if ($isReferer && request()->server('HTTP_REFERER')) {
            $url = request()->server('HTTP_REFERER');
        } else {
            $url = request()->fullUrl();
        }

        $urlRaiz = request()->root();
        $urlActive = str_replace($urlRaiz . '/', '', $url);
        return $urlActive;

    }

    public static function isActiveRoute($route, $output = 'active')
    {

        $url = request()->fullUrl();
        $urlRaiz = request()->root();
        $urlActive = static::urlActive();
        $route = str_replace($urlRaiz . '/', '', static::urlEncode($route));
        if (rawurldecode($urlActive) == $route) {
            return $output;
        }
    }

    public static function numberFormat($numero, $decimals = 2, $separator_decimal = ".", $separator_miles = ",")
    {
        if ($numero == "" || $numero == null) {
            $numero = 0;
        }
        return number_format($numero, $decimals, $separator_decimal, $separator_miles);
    }

    public static function validRuc($ruc) //validacion sunat
    {
        $ruc = trim($ruc);
        if ($ruc) {
            if (strlen($ruc) == 11) {
                $suma = 0;
                $x = 6;
                for ($i = 0; $i < strlen($ruc) - 1; $i++) {
                    if ($i == 4) {
                        $x = 8;
                    }
                    $digito = $ruc[$i];
                    $x--;
                    if ($i == 0) {
                        $suma += ($digito * $x);
                    } else {
                        $suma += ($digito * $x);
                    }
                }
                $resto = $suma % 11;
                $resto = 11 - $resto;
                if ($resto >= 10) {
                    $resto = $resto - 10;
                }
                if ($resto == $ruc[strlen($ruc) - 1]) {
                    return true;
                }
            }
        }
        return false;
    }

    public static function getModifiedJs($path = null)
    {
        if (is_null($path)) {
            $path = public_path() . '/js/';
        }
        return static::getModifiedTime($path, 'js');
    }

    public static function getModifiedScript($path = null)
    {
        if (is_null($path)) {
            $path = public_path() . '/script/';
        }
        return static::getModifiedTime($path, 'js');
    }

    public static function getModifiedcss($path)
    {
        if (is_null($path)) {
            $path = public_path() . '/css';
        }
        return static::getModifiedTime($path, 'css');
    }

    public static function getModifiedTime($path, $type)
    {
        $finder = new \Symfony\Component\Finder\Finder();
        $finder->in($path)->name('*.' . $type);
        $latest = 0;
        foreach ($finder as $file) {
            $mtime = filemtime($file->getRealPath());
            if ($mtime > $latest) {
                $latest = $mtime;
            }
        }
        return $latest;

    }

    public static function urlEncode($url) //los generics
    {
        //generic?modelo=Model
        if (strpos($url, 'modelo') !== false) {
            list($generic, $modelo) = explode('modelo', $url);
            $modelo = str_replace('=', '', $modelo);
            $modelo = base64_encode($modelo);
            $url = 'generic?modelo=' . $modelo;
        }
        return url($url);
    }

    public static function urlDecode($url) //los generics
    {
        //generic?modelo=Model
        if (strpos($url, 'modelo') !== false) {
            list($generic, $modelo) = explode('modelo', $url);
            $modelo = str_replace('=', '', $modelo);
            $modelo = base64_decode($modelo);
            $url = 'generic?modelo=' . $modelo;
        }
        return url($url);
    }

    public static function formatoMonthYear($value)// formato MM/YYYY
    {
        if (strlen($value) != 6) {
            return false;
        }
        $mes = substr($value, 0, 2);
        $anio = substr($value, 2, 4);
        return checkdate($mes, 10, $anio);
    }

    public static function getCompletaCeros($caracter, $numerocaracteres, $caracterarellenar, $tipo = 'I') //I:IZQUIERDA ; D:DERECHA
    {
        if ($tipo == 'I') {
            return str_pad($caracter, $numerocaracteres, $caracterarellenar, STR_PAD_LEFT);
        } else {
            return str_pad($caracter, $numerocaracteres, $caracterarellenar);
        }
    }

    public static function getMeses()
    {
        return array('ENERO', 'FEBRERO', 'MARZO', 'ABRIL', 'MAYO', 'JUNIO', 'JULIO', 'AGOSTO', 'SEPTIEMBRE', 'OCTUBRE', 'NOVIEMBRE', 'DICIEMBRE');
    }

    public static function convertDate($split, $join, $fecha)
    {
        $cadena = (string)$fecha;

        if (strpos($cadena, $split) === false) {
            return $cadena;
        }

        $fecha = explode($split, $fecha);
        $fecha = array_reverse($fecha);
        $cadena = implode($join, $fecha);
        return $cadena;

    }

    public static function validateDate($date, $format = 'Y-m-d H:i:s')
    {
        $d = \DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }

    public static function ejecutarConsulta($sql, $parametros = array(), $devolver_uno_o_null = false)
    {

        $resultado = \DB::select($sql, $parametros);
        if ($devolver_uno_o_null == true) {
            if (count($resultado) > 0) {
                return $resultado[0];
            }
            return null;
        }
        return $resultado;

    }

    public static function removeSpace($texto)
    {
        return preg_replace('/\s+/', '', $texto);
    }

    public static function nominalRate($cycle, $number, $tea)
    {
        $dias_anio = 360;
        switch ($cycle) {
            case 'D':
                $expo = (int)($dias_anio / $number);
                $text = 'day';
                break;
            case 'M':
                $expo = (int)(12 / $number);
                $text = 'month';
                break;
            case 'A':
                $expo = 1;
                $text = 'year';
                break;
        }
        $rate_cuota = round(pow((100 + $tea) / 100, (1 / $expo)) - 1, 6);
        return array($rate_cuota, $text);
    }

    public static function sortBy($array, $key, $order = SORT_ASC, $options = SORT_LOCALE_STRING)
    {
        $column = array_column($array, $key);
        array_multisort($column, $order, $options, $array);
        return $array;
    }

    public static function sum($array, $key, $callback = null)
    {
        $sum = 0;
        if (func_num_args() == 2 && is_callable($key)) {
            $callback = $key;
        }
        if (is_null($callback)) {
            $sum = array_sum(array_column($array, $key));
        }
        if (is_callable($callback)) {
            $sum = array_sum(array_map($callback, $array));
        }
        return $sum;
    }

    public static function sumRecursive($array, $key)
    {
        $sum = array_sum(
            iterator_to_array(
                new \RegexIterator(
                    new \RecursiveIteratorIterator(
                        new \RecursiveArrayIterator($array)
                    ),
                    '/^' . $key . '$/D',
                    \RegexIterator::MATCH,
                    \RegexIterator::USE_KEY
                ),
                false
            )
        );
        return $sum;
    }

    public static function groupByArray($array, $indice)
    {
        $groupedById = array_reduce($array, function (array $accumulator, array $element) use ($indice) {
            $accumulator[$element[$indice]][] = $element;
            return $accumulator;
        }, array());
        return $groupedById;
    }
    public function pluck($array,$key)
    {
        $keys=[];
        foreach($array as $value){
            if (! is_null($item = $value[$key])) {
                $keys[] = $item;
            }
        }
        if (count($keys) === 0) {
            return [0];
        }
        sort($keys);
        return array_values(array_unique($keys));
    }
}